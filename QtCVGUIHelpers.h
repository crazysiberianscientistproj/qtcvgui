#ifndef QTCVGUIHELPERS_H
#define QTCVGUIHELPERS_H

#include <QList>

#include <functional>

class QImage;
class QPolygon;

class QtCVGUIHelpers {

public:

    /*!
     * \brief ProcFunc
     * Костыль для того, чтобы можно было сделать std::function<void <const...> аргументом сигнала или слота
     * Подробнее смотри в:
     * https://forum.qt.io/topic/78860/using-std-function-as-parameter-for-slot-and-signal/6
     * https://bugreports.qt.io/browse/QTBUG-60539
     */
    typedef std::function<void (const QImage &, QList<QPolygon> &, QList<QImage> &)> ProcFunc;

    static QtCVGUIHelpers& instance();

private:
    QtCVGUIHelpers();

};

#endif // QTCVGUIHELPERS_H
