#ifndef QTCVGUI_H
#define QTCVGUI_H

#include <QMainWindow>
#include <QScopedPointer>
#include <QPixmap>
#include <QPolygon>
#include <QList>
#include <QThread>
#include <QMetaType>
#include <QHash>

#include <atomic>
#include <functional>

#include "PropertyBrowserWidget.h"
#include "QtImageViewer.h"
#include "ImageProcessor.h"
#include "QtCVGUIHelpers.h"
#include "FileFilters.h"

class Capture;

namespace Ui {
class QtCVGUI;
}

class QtCVGUI : public QMainWindow
{
    Q_OBJECT

public:
    enum CaptureInputType
    {
        INPUT_SINGLE,
        INPUT_SEQENCE,
        INPUT_DEVICE
    };

    enum CaptureOpenDialogType
    {
        DIALOG_FILE,
        DIALOG_FOLDER,
        DIALOG_DEVICE
    };

private:
    typedef std::function<Capture*(const QString &, QObject *)> CaptureFabric;

    struct CaptureData
    {
//        QString filter_;
        CaptureFabric newCapture;
        CaptureInputType inputType_;
//        CaptureOpenDialogType dialogType_;

        CaptureData(//const QString &filter
                    CaptureFabric captureFabric
                    , CaptureInputType inputType
//                  , CaptureOpenDialogType dialogType
                    ):
//              filter_(filter),
              newCapture(captureFabric)
              , inputType_(inputType)
//            , dialogType_(dialogType)
        {}

        CaptureData(){}
    };


public:
    explicit QtCVGUI(QWidget *parent = 0, bool doAddDefaultCaptures = true);
    ~QtCVGUI();

    void addPropertyBrowser(PropertyBrowserWidget *propertyBrowser);
    void setProcFunc(const QtCVGUIHelpers::ProcFunc &procFunc);
    template<typename T>
    void addCapture(CaptureInputType inputType, CaptureOpenDialogType dialogType,
                             const QString &captureName, const QStringList &extList = QStringList());

private:
    void closeEvent(QCloseEvent *event) override;

    template<typename T> CaptureFabric makeCaptureFabric();

    void stopPlay();
    void setImageProcessorWorkMode(bool isRun);
    void openFile(CaptureOpenDialogType dialogType);
    void addDefaultCaptures();
    void initPlaybackPanel();
    void initForm();
    void initConnections();
    void initScene();
    void initOpenFilesPanel();

signals:
    void sendSetFrame(int frameNum);
    void sendSetCapture(Capture *capture);
    void sendSetProcFunc(const QtCVGUIHelpers::ProcFunc & procFunc);


private slots:
    void onOpenActionTriggered(bool);
    void onOpenImgSeqTriggered(bool);
    void onSetFileName(const QString & fileName);
    void onReceiveFrame(const QImage &frame, int frameNum);
    void onParamChanged();
    void on_playButton_clicked(bool checked);
    void on_actionOpenConfig_triggered(bool checked);
    void on_actionSaveAsConfig_triggered(bool checked);
    void on_actionSaveConfig_triggered(bool checked);

    void on_infoButton_clicked();

private:
    Ui::QtCVGUI *ui;

    /// \todo Надо бы потом обдумать как удобнее этот объект определять:
    /// как подчинённого (child) у какого-нибудь класса или как независимого
    QScopedPointer<Capture> capture_;
    QHash<CaptureOpenDialogType, QHash<QString, CaptureData>> capturesFabrics_;
    bool openFileDlgFltrNeedsUpdt_;
    QString openFileDlgFltr_;

    QString fileFilters_;

    ImageProcessor imageProcessor_;
    QThread imageProcessorThread_;

    /// \todo Не нравятся мне эти флаги, надо бы потом поумнее сделать
    std::atomic_bool paramSetFlag_;

    QString lastSavedConfig_;

};

template<typename T>  QtCVGUI::CaptureFabric QtCVGUI::makeCaptureFabric()
{
    return
    [](const QString &path, QObject *parent=0)-> Capture* {
        return new T(path, parent);
    };
}

template<typename T>
void QtCVGUI::addCapture(QtCVGUI::CaptureInputType inputType, QtCVGUI::CaptureOpenDialogType dialogType,
                         const QString &captureName, const QStringList &extList)
{
    capturesFabrics_[dialogType][FileFilters::getFilesFilter(extList, captureName)]
            = CaptureData(this->makeCaptureFabric<T>(), inputType);

    if(dialogType == DIALOG_FILE) openFileDlgFltrNeedsUpdt_=true;
}

#endif // QTCVGUI_H
