#include <QDebug>

#include "FileFilters.h"

/*static*/ const QStringList FileFilters::IMAGESEXT=QStringList({"*.png","*.bmp","*.jpg","*.jpeg"});

/*static*/ const QString FileFilters::IMAGES=getFilesFilter(IMAGESEXT, "Images");
/*static*/ const QString FileFilters::VIDEO="Video (*.avi *.mp4)";

/*static*/ const QString FileFilters::ALL=FileFilters::IMAGES
        + ";;" + FileFilters::VIDEO;

QString FileFilters::getFilesFilter(const QStringList &extList, const QString &filterName)
{
    if(extList.size() == 0) return filterName;

    QString str=filterName + " (";
    for(auto &ext:extList)
        str+=ext+ " ";
    *(str.end()-1)=')';

    return str;
}
