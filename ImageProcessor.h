#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

#include <QObject>
#include <QImage>
#include <QDebug>
#include <QMutex>
#include <QWaitCondition>

#include <functional>
#include <atomic>

#include "Capture/Capture.h"
#include "QtCVGUIHelpers.h"

class ImageProcessor : public QObject
{
    Q_OBJECT


    static const double PLAY_TIMER_PERIOD;

public:

    ImageProcessor(Capture *capture=0, QObject *parent=0);
    ~ImageProcessor();


public slots:
    void onSetCapture(Capture *capture);
    void onSetProcFunc(const QtCVGUIHelpers::ProcFunc & procFunc);

    void onProcFrame();
    void onSetFrame(int num);
    void onPlay(bool playFlag);
    void onProcEnable(bool flag);

signals:
    void sendFileName(const QString &);
    void sendFrame(const QImage &image, int frameNum);
    void procIsRun(bool);

private:
    void procFrame();
    void timerEvent(QTimerEvent *event) override;

    QtCVGUIHelpers::ProcFunc procFunc_;
    Capture *capture_;
    QImage frame_;
    QString lastFileName_;
    std::atomic_bool procIsEnabled_;
    int playTimerId_;

};

#endif // IMAGEPROCESSOR_H
