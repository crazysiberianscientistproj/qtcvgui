#ifndef CAPTURELISTDIALOG_H
#define CAPTURELISTDIALOG_H

#include <QDialog>

class QListWidget;

class CaptureListDialog : public QDialog
{
    Q_OBJECT
public:
    explicit CaptureListDialog(QWidget *parent = 0,
                               const QString &dir = QString(),
                               const QList<QString> &filters = QList<QString>());

    const QString &lastSelectedFilter() const;
    const QString &lastSelectedFolder() const;
    void addFilters(const QList<QString> &filters);

    static QString getExistingDirectory(QWidget *parent = Q_NULLPTR,
                                const QString &dir = QString(),
                                const QList<QString> &filters = QList<QString>(),
                                QString *selectedFilter = Q_NULLPTR);

private:
    void initForm();

private slots:
    void onOkClicked();
    void onCancelClicked();

private:
    QListWidget *list_;
    QString lastSelectedFilter_, lastSelectedFolder_, openDir_;

};

#endif // CAPTURELISTDIALOG_H
