#include <QMutexLocker>
#include <QTimerEvent>

#include "ImageProcessor.h"


/*static*/ const double ImageProcessor::PLAY_TIMER_PERIOD=0;

ImageProcessor::ImageProcessor(Capture *capture, QObject *parent):
    QObject(parent)
  , capture_(capture)
  , procIsEnabled_(false)
{

}

ImageProcessor::~ImageProcessor()
{
}

void ImageProcessor::onProcFrame()
{
    if(capture_==0 || procFunc_==nullptr)return;

    emit procIsRun(true);
    QList<QPolygon> polygonList;
    QList<QImage> imageList;
    if(procIsEnabled_)
        procFunc_(frame_, polygonList, imageList);
    emit procIsRun(false);
}

void ImageProcessor::onSetFrame(int num)
{
    if(capture_==0)return;

    capture_->setCurrFrameNum(num);
    this->procFrame();
}

void ImageProcessor::onPlay(bool playFlag)
{
    if(capture_==0)return;

    if(playFlag)
        playTimerId_ = this->startTimer(PLAY_TIMER_PERIOD);
    else
        this->killTimer(playTimerId_);
}

void ImageProcessor::onProcEnable(bool flag)
{
    procIsEnabled_=flag;
    this->onProcFrame();
}

void ImageProcessor::procFrame()
{
    int frameN=capture_->getCurrFrameNum();

    QString fName=capture_->getFileName();
    frame_=capture_->getFrame();

    emit sendFrame(frame_, frameN);
    if(lastFileName_!=fName)
    {
        lastFileName_=fName;
        emit sendFileName(fName);
    }

    this->onProcFrame();
}

void ImageProcessor::timerEvent(QTimerEvent *event)
{
    if(event->timerId() == playTimerId_){
        this->procFrame();
    }
}

void ImageProcessor::onSetCapture(Capture *capture)
{
    capture_ = capture;
}

void ImageProcessor::onSetProcFunc(const QtCVGUIHelpers::ProcFunc & procFunc)
//void ImageProcessor::onSetProcFunc(const std::function<void (/*const*/ QImage &, QList<QPolygon> &, QList<QImage> &)> &procFunc)
{
    qDebug()<<Q_FUNC_INFO;
    procFunc_ = procFunc;
}
