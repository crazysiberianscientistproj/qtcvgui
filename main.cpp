#include <QApplication>
#include <QLayout>
#include <QThread>

#include <PropertyBrowserWidget.h>

#include "QtCVGUI.h"

struct Baka
{

    double d;
    int i;
    bool b;

    Baka(){
        d=-0.3;
        i=-7;
        b=false;
    }

    void setD(double v)
    {
        d=v;
        qDebug()<<"setD:"<<d<<endl;
    }

    double getD() const
    {
        return d;
    }

    void setI(int v)
    {
        i=v;
        qDebug()<<"setI:"<<i<<endl;
    }

    int getI() const
    {
        qDebug()<<"getI:"<<i<<endl;
        return i;
    }

    void setB(bool v)
    {
        b=v;
        qDebug()<<"setB:"<<b<<endl;
    }

    bool getB() const
    {
        return b;
    }
} baka, baka1 ,baka2;

void bakaProcFunc(const QImage &, QList<QPolygon> &, QList<QImage> &)
{
    qDebug()<<Q_FUNC_INFO;
    QThread::msleep(1000);
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QtCVGUI w;
    w.show();

    PropertyBrowserWidget *pbw = new PropertyBrowserWidget;
    pbw->setTitle("Baka");
    pbw->addProperty(new Property<int>("I",&Baka::getI, &Baka::setI,baka));
    pbw->addProperty(new Property<double>("D",&Baka::getD, &Baka::setD,baka));

    w.addPropertyBrowser(pbw);

    pbw= new PropertyBrowserWidget;
    pbw->addProperty(new Property<int>("I",&Baka::getI, &Baka::setI,baka1));
    pbw->addProperty(new Property<double>("D",&Baka::getD, &Baka::setD,baka1));
    pbw->setTitle("Baka1");

    w.addPropertyBrowser(pbw);

    pbw= new PropertyBrowserWidget;
    pbw->addProperty(new Property<int>("I",&Baka::getI, &Baka::setI,baka2));
    pbw->addProperty(new Property<double>("D",&Baka::getD, &Baka::setD,baka2));
    pbw->setTitle("Baka2");

    w.addPropertyBrowser(pbw);

    w.setProcFunc(bakaProcFunc);

    return a.exec();
}
