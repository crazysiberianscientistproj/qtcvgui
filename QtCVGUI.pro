#-------------------------------------------------
#
# Project created by QtCreator 2017-02-18T15:09:06
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtCVGUI
TEMPLATE = app


include(../QtPropertyBrowser/QtPropertyBrowser.pri)
include(../QtImageViewer/QtImageViewer.pri)

INCLUDEPATH += \
    ../QtPropertyBrowser \
    ../QtImageViewer

SOURCES += \
    main.cpp \
    QtCVGUI.cpp \
    FileFilters.cpp \
    ImageProcessor.cpp \
    Capture/ImageSeqCapture.cpp \
    Capture/Capture.cpp \
    Capture/ImageCapture.cpp \
    QtCVGUIHelpers.cpp \
    CaptureListDialog.cpp

HEADERS  += \
    QtCVGUI.h \
    FileFilters.h \
    ImageProcessor.h \
    Capture/ImageSeqCapture.h \
    Capture/Capture.h \
    Capture/ImageCapture.h \
    QtCVGUIHelpers.h \
    CaptureListDialog.h

FORMS    += QtCVGUI.ui

RESOURCES += \
    resources.qrc
