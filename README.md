# README #

## РУССКИЙ ##

Привет, товарищ!


Это проект графического пользовательского интерфейса для приложений, связанных с обработкой изображений.

Тут ещё много чего надо сделать, но он его уже можно использовать для своих делишек.

Пример использования смотри в main.cpp .


Требования:

* Qt 5.7 версии или новее. Возможно на более старой тоже будет работать, но я не пробовал.

* Компилятор с поддержкой C++11 (хотя в настройках проекта стоит C++14, но должно работать и на C++11)


Проект связан с другими и не сможет собраться без них:

* [QtPropertyBrowser](https://bitbucket.org/crazysiberianscientistproj/qtpropertybrowser)

* [QtImageViewer](https://bitbucket.org/crazysiberianscientistproj/qtimageviewer)


### Контакты ###
crazysiberianscientist@gmail.com




## ENGLISH ##

Hi, comrade!

This is GUI project for image processing applications. 

There is still a lot to do, but you can use it.

Using example see in main.cpp . 


Requirements:

* Qt 5.7 version or newer. Maybe this might works on older version, but I didn't try.

* Compiler with C++11 support (although, in project settings C++14 is setted, this might works on C++11).

This project is linked with others and can't be builded without them:

* [QtPropertyBrowser](https://bitbucket.org/crazysiberianscientistproj/qtpropertybrowser)

* [QtImageViewer](https://bitbucket.org/crazysiberianscientistproj/qtimageviewer)


### Contacts ###
crazysiberianscientist@gmail.com