#ifndef FILEFILTERS_H
#define FILEFILTERS_H

#include <QString>
#include <QStringList>

class FileFilters {
public:

    static QString getFilesFilter(const QStringList& extList, const QString &filterName);

    static const QStringList IMAGESEXT;

    static const QString IMAGES;
    static const QString VIDEO;

    static const QString ALL;

};

#endif // FILEFILTERS_H
