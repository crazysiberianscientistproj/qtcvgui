#include "QtCVGUIHelpers.h"

#include <QMetaType>

QtCVGUIHelpers& QtCVGUIHelpers::instance()
{
    static QtCVGUIHelpers qtCVGUIHelpers;
    return qtCVGUIHelpers;
}

QtCVGUIHelpers::QtCVGUIHelpers()
{
    qRegisterMetaType<ProcFunc>("QtCVGUIHelpers::ProcFunc");
}
