#-------------------------------------------------
#
# Project created by QtCreator 2017-02-18T15:09:06
#
#-------------------------------------------------

#QT       += core gui
#CONFIG   += c++14

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#TARGET = QtCVGUI
#TEMPLATE = app


include(../QtPropertyBrowser/QtPropertyBrowser.pri)
include(../QtImageViewer/QtImageViewer.pri)

INCLUDEPATH += \
    $$PWD/../QtPropertyBrowser \
    $$PWD/../QtImageViewer

SOURCES += \
    $$PWD/QtCVGUI.cpp \
    $$PWD/FileFilters.cpp \
    $$PWD/ImageProcessor.cpp \
    $$PWD/Capture/ImageSeqCapture.cpp \
    $$PWD/Capture/Capture.cpp \
    $$PWD/Capture/ImageCapture.cpp \
    $$PWD/QtCVGUIHelpers.cpp \
    $$PWD/CaptureListDialog.cpp

HEADERS  += \
    $$PWD/QtCVGUI.h \
    $$PWD/FileFilters.h \
    $$PWD/ImageProcessor.h \
    $$PWD/Capture/ImageSeqCapture.h \
    $$PWD/Capture/Capture.h \
    $$PWD/Capture/ImageCapture.h \
    $$PWD/QtCVGUIHelpers.h \
    $$PWD/CaptureListDialog.h

FORMS    += $$PWD/QtCVGUI.ui

RESOURCES += \
    $$PWD/resources.qrc
