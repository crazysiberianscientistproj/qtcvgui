#include "CaptureListDialog.h"

#include <QListWidget>
#include <QLayout>
#include <QDialogButtonBox>
#include <QFileDialog>

CaptureListDialog::CaptureListDialog(QWidget *parent,
                                     const QString &dir,
                                     const QList<QString> &filters):
    QDialog(parent)
{
    this->initForm();
    this->addFilters(filters);
    openDir_ = dir;
}

const QString & CaptureListDialog::lastSelectedFilter() const
{
    return lastSelectedFilter_;
}

const QString &CaptureListDialog::lastSelectedFolder() const
{
    return lastSelectedFolder_;
}

void CaptureListDialog::addFilters(const QList<QString> &filters)
{
    list_->addItems(filters);
    if(filters.size()) list_->item(0)->setSelected(true);
}

QString CaptureListDialog::getExistingDirectory(QWidget *parent, const QString &dir, const QList<QString> &filters, QString *selectedFilter)
{
    CaptureListDialog *cls = new CaptureListDialog(parent, dir, filters);
    cls->exec();

    *selectedFilter = cls->lastSelectedFilter();
    auto folder = cls->lastSelectedFolder();
    cls->setParent(0);
    delete cls;

    return folder;
}

void CaptureListDialog::initForm()
{
    this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QVBoxLayout *mainLayout = new QVBoxLayout(this);

    list_ = new QListWidget;
    list_->setSelectionMode(QAbstractItemView::SingleSelection);
    mainLayout->addWidget(list_);

    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Ok
                                                     | QDialogButtonBox::Cancel
                                                     , Qt::Horizontal);
    connect(buttons, SIGNAL(accepted()), this, SLOT(onOkClicked()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(onCancelClicked()));

    mainLayout->addWidget(buttons);
}

void CaptureListDialog::onOkClicked()
{
    if(list_->count()==0) return;
    lastSelectedFilter_ = list_->selectedItems()[0]->text();
    lastSelectedFolder_ = QFileDialog::getExistingDirectory(this,
                                      "Select folder", openDir_);
    if(lastSelectedFolder_.isEmpty()) return;

    this->close();
    emit this->accepted();
}

void CaptureListDialog::onCancelClicked()
{
    this->close();
    emit this->rejected();
}
