#include "QtCVGUI.h"
#include "ui_QtCVGUI.h"

#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QThread>
#include <QtConcurrent/QtConcurrent>
#include <QToolBar>
#include <QMessageBox>

#include "Capture/ImageSeqCapture.h"
#include "Capture/ImageCapture.h"
#include "ImageProcessor.h"
#include "CaptureListDialog.h"

QtCVGUI::QtCVGUI(QWidget *parent, bool doAddDefaultCaptures) :
    QMainWindow(parent)
  , ui(new Ui::QtCVGUI)
  , paramSetFlag_(false)
{
    ui->setupUi(this);

    QtCVGUIHelpers::instance();

    this->initForm();
    this->initConnections();

    imageProcessor_.moveToThread(&imageProcessorThread_);

    if(doAddDefaultCaptures)
        this->addDefaultCaptures();
}

QtCVGUI::~QtCVGUI()
{
    delete ui;
}

void QtCVGUI::addPropertyBrowser(PropertyBrowserWidget *propertyBrowser)
{
    ui->propertyListWidget->layout()->addWidget(propertyBrowser);

    connect(&imageProcessor_, &ImageProcessor::procIsRun, propertyBrowser, &PropertyBrowserWidget::setDisabled);
    connect(propertyBrowser, &PropertyBrowserWidget::valueChanged, &imageProcessor_, &ImageProcessor::onProcFrame);
}

void QtCVGUI::setProcFunc(const QtCVGUIHelpers::ProcFunc &procFunc)
{
    qDebug()<<Q_FUNC_INFO;
    emit this->sendSetProcFunc(procFunc);
}

void QtCVGUI::closeEvent(QCloseEvent *event)
{
    imageProcessorThread_.quit();
//    imageProcessorThread_.terminate();
    imageProcessorThread_.wait();
}

void QtCVGUI::onOpenActionTriggered(bool)
{
    this->openFile(DIALOG_FILE);
}

void QtCVGUI::onOpenImgSeqTriggered(bool)
{
    this->openFile(DIALOG_FOLDER);
}

void QtCVGUI::onSetFileName(const QString & fileName)
{
    ui->fileName->setText(fileName);
}

void QtCVGUI::onReceiveFrame(const QImage &frame, int frameNum)
{
    ui->imageViewer->onSetImage(frame);

    ui->currFrameNum->blockSignals(true);
    ui->playSlider->blockSignals(true);
    ui->currFrameNum->setValue(frameNum);
    ui->playSlider->setValue(frameNum);

    ui->currFrameNum->blockSignals(false);
    ui->playSlider->blockSignals(false);

}

void QtCVGUI::onParamChanged()
{
    paramSetFlag_=true;
}

void QtCVGUI::stopPlay()
{
    if(ui->playButton->isChecked())
        ui->playButton->click();
}

void QtCVGUI::setImageProcessorWorkMode(bool isRun)
{
    if(isRun){
        imageProcessorThread_.start();
        return;
    }

    imageProcessorThread_.quit();
    imageProcessorThread_.wait();
}

void QtCVGUI::openFile(QtCVGUI::CaptureOpenDialogType dialogType)
{
    this->stopPlay();
    ui->procCheckBox->setChecked(false);
    this->setImageProcessorWorkMode(false);

    QString selectedFilter, path;
    QHash<QString, CaptureData> *capturesFabric = &capturesFabrics_[dialogType];

    QList<QString> filters = capturesFabric->keys();
    if(filters.size()==0)
    {
        QMessageBox::warning(this, "Open file dialog error", "Any file filters don't exist");
        return;
    }

    if(dialogType == DIALOG_FILE){
        if(openFileDlgFltrNeedsUpdt_){
            openFileDlgFltrNeedsUpdt_ = false;
            openFileDlgFltr_.clear();

            auto it=filters.begin(), ite=filters.end()-1;
            for(;it!=ite;++it)
                openFileDlgFltr_ += *it + ";;";
            openFileDlgFltr_ += *it;
        }

        path = QFileDialog::getOpenFileName(
                        this,
                        "Open file",
                        QString(),
                        openFileDlgFltr_,
                        &selectedFilter);
    }
    else if (dialogType == DIALOG_FOLDER){

        path = CaptureListDialog::getExistingDirectory(this,QString(),filters, &selectedFilter);
    }
    else if (dialogType == DIALOG_DEVICE){

    }
    if (path.isEmpty() || selectedFilter.isEmpty()) return;

    CaptureData *captureData = &(*capturesFabric)[selectedFilter];
    capture_.reset(captureData->newCapture(path,0));
    if(capture_->getFramesNum()==0) {
        capture_.reset(0);
        ui->playbackPanel->hide();
        return;
    }

    if(captureData->inputType_ == INPUT_SEQENCE)
        this->initPlaybackPanel();
    else
        ui->playbackPanel->hide();

    this->setImageProcessorWorkMode(true);
    emit this->sendSetCapture(capture_.data());
    emit this->sendSetFrame(1);

}

void QtCVGUI::addDefaultCaptures()
{
    this->addCapture<ImageCapture>(INPUT_SINGLE, DIALOG_FILE, "Images", FileFilters::IMAGESEXT);
    this->addCapture<ImageSeqCapture>(INPUT_SEQENCE, DIALOG_FOLDER, "Images sequence");
}

void QtCVGUI::initPlaybackPanel()
{
    ui->playbackPanel->show();
    ui->framesNum->setText("/" + QString::number(capture_->getFramesNum()));
    ui->currFrameNum->setMaximum(capture_->getFramesNum());
    ui->playSlider->setMaximum(capture_->getFramesNum());
}

void QtCVGUI::initForm()
{
    // Playback panel
    ui->playbackPanel->hide();
    ui->playButton->setIconSize(QSize(24,24));
    ui->playButton->setIcon(this->style()->standardIcon(QStyle::SP_MediaPlay));
    ui->playButton->setShortcut(QKeySequence("SPACE"));

    this->initScene();
    this->initOpenFilesPanel();


}

void QtCVGUI::initConnections()
{
    /// \todo Мешать стили подключений не очень хорошо, но всё переделать на вид через указатели на методы пока лень
    connect(ui->playButton,SIGNAL(clicked(bool)),&imageProcessor_,SLOT(onPlay(bool)));
    connect(ui->currFrameNum,SIGNAL(valueChanged(int)),&imageProcessor_,SLOT(onSetFrame(int)));
    connect(ui->playSlider,SIGNAL(valueChanged(int)),&imageProcessor_,SLOT(onSetFrame(int)));
    connect(this, &QtCVGUI::sendSetFrame, &imageProcessor_, &ImageProcessor::onSetFrame);
    connect(this, &QtCVGUI::sendSetCapture, &imageProcessor_, &ImageProcessor::onSetCapture);
    connect(this, &QtCVGUI::sendSetProcFunc, &imageProcessor_, &ImageProcessor::onSetProcFunc);
    connect(&imageProcessor_,SIGNAL(sendFrame(QImage ,int)),this,SLOT(onReceiveFrame(QImage, int)));
    connect(&imageProcessor_,SIGNAL(sendFileName(QString)),this,SLOT(onSetFileName(QString)));

    connect(ui->actionOpen,SIGNAL(triggered(bool)),this,SLOT(onOpenActionTriggered(bool)));
    connect(ui->actionOpen_image_seqence,SIGNAL(triggered(bool)),this,SLOT(onOpenImgSeqTriggered(bool)));
    connect(ui->procCheckBox,SIGNAL(toggled(bool)),&imageProcessor_,SLOT(onProcEnable(bool)));

}

void QtCVGUI::initScene()
{
    dynamic_cast<QBoxLayout*>(ui->propertyListWidget->layout())->setDirection(QBoxLayout::BottomToTop);
}

void QtCVGUI::initOpenFilesPanel()
{
    ui->openFileButton->setIcon(QPixmap("://resources/image.png"));
    connect(ui->openFileButton,SIGNAL(clicked(bool)),this,SLOT(onOpenActionTriggered(bool)));
    ui->openImgSeqButton->setIcon(QPixmap("://resources/images.png"));
    connect(ui->openImgSeqButton,SIGNAL(clicked(bool)),this,SLOT(onOpenImgSeqTriggered(bool)));
    ui->openConfigButton->setIcon(QPixmap("://resources/preferences.png"));
    connect(ui->openConfigButton,SIGNAL(clicked(bool)),this,SLOT(on_actionOpenConfig_triggered(bool)));
    ui->saveConfigButton->setIcon(QPixmap("://resources/preferencesSave.png"));
    connect(ui->saveConfigButton,SIGNAL(clicked(bool)),this,SLOT(on_actionSaveConfig_triggered(bool)));
    ui->saveAsConfigButton->setIcon(QPixmap("://resources/preferencesSaveAs.png"));
    connect(ui->saveAsConfigButton,SIGNAL(clicked(bool)),this,SLOT(on_actionSaveAsConfig_triggered(bool)));
    ui->infoButton->setIcon(this->style()->standardIcon(QStyle::SP_MessageBoxInformation));


    QToolBar *toolBar = new QToolBar;
    ui->toolPanel->insertWidget(0,toolBar);
    toolBar->addWidget(ui->openFileButton);
    toolBar->addWidget(ui->openImgSeqButton);
    toolBar->addSeparator();
    toolBar->addWidget(ui->openConfigButton);
    toolBar->addWidget(ui->saveConfigButton);
    toolBar->addWidget(ui->saveAsConfigButton);
}

void QtCVGUI::on_playButton_clicked(bool checked)
{
    if(checked)
    {
        ui->currFrameNum->blockSignals(true);
        ui->playSlider->blockSignals(true);
        ui->currFrameNum->setEnabled(false);
        ui->playSlider->setEnabled(false);
        ui->playButton->setIcon(this->style()->standardIcon(QStyle::SP_MediaPause));
        return;
    }
    ui->currFrameNum->blockSignals(false);
    ui->playSlider->blockSignals(false);
    ui->currFrameNum->setEnabled(true);
    ui->playSlider->setEnabled(true);
    ui->playButton->setIcon(this->style()->standardIcon(QStyle::SP_MediaPlay));
}

void QtCVGUI::on_actionOpenConfig_triggered(bool checked)
{
    Q_UNUSED(checked)
    this->stopPlay();

    QString selectedFilter;
    QString fileName = QFileDialog::getOpenFileName(
                this,
                "Open config",
                QString(),
                "JSON (*.json)",
                &selectedFilter);
    if(fileName.isEmpty())return;

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open config.");
        return;
    }

    QByteArray saveData = file.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonObject jsonObj=(loadDoc.object());

    foreach(PropertyBrowserWidget *widget, ui->propertyListWidget->findChildren<PropertyBrowserWidget*>())
    {
       widget->read(jsonObj);
    }
    file.close();
}

void QtCVGUI::on_actionSaveAsConfig_triggered(bool checked)
{
    Q_UNUSED(checked)
    this->stopPlay();

    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName(
                this,
                "Save config",
                QString(),
                "JSON (*.json)",
                &selectedFilter);
    if(fileName.isEmpty())return;


    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't save config.");
        return;
    }
    lastSavedConfig_=fileName;

    QJsonObject jsonObj;
    foreach(PropertyBrowserWidget *widget, ui->propertyListWidget->findChildren<PropertyBrowserWidget*>())
    {
       widget->write(jsonObj);
    }

    QJsonDocument doc(jsonObj);
    file.write(doc.toJson());
    file.close();
}

void QtCVGUI::on_actionSaveConfig_triggered(bool checked)
{
    this->stopPlay();

    if(lastSavedConfig_.isEmpty())
        this->on_actionSaveAsConfig_triggered(checked);

    QFile file(lastSavedConfig_);
    if (!file.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't save config.");
        return;
    }

    QJsonObject jsonObj;
    foreach(PropertyBrowserWidget *widget, ui->propertyListWidget->findChildren<PropertyBrowserWidget*>())
    {
       widget->write(jsonObj);
    }

    QJsonDocument doc(jsonObj);
    file.write(doc.toJson());
    file.close();
}

void QtCVGUI::on_infoButton_clicked()
{
    QString message=QString::fromUtf8("Сделано Морозом Юрием\n"
                                      "Created by Moroz Yuri\n"
                                      "crazysiberianscientist@gmail.com\n"
                                      "https://bitbucket.org/CrazySiberianScientist");
    QMessageBox::information(0,"Info",message);
}
