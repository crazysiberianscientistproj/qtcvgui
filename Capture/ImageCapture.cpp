#include "ImageCapture.h"

ImageCapture::ImageCapture(const QString &path, QObject *parent):
    Capture(path, parent)
{
    this->open(path);
}

bool ImageCapture::open(const QString &path)
{
    frame_=QImage(path);
    if(frame_.isNull())return false;
    framesNum_=1;

    fileName_=path;
    return true;
}

QImage ImageCapture::getFrame()
{
    return frame_;
}
