#ifndef CAPTURE_H
#define CAPTURE_H

#include <QObject>
#include <QImage>

/*!
 * \brief The Capture class
 */

class Capture : public QObject
{
    Q_OBJECT

public:
    Capture(const QString &path, QObject *parent=0);
    virtual ~Capture();

    virtual bool open(const QString &path)=0;
    virtual QImage getFrame() = 0;
    virtual int getCurrFrameNum() const;
    virtual void setCurrFrameNum(int currFrameNum);
    virtual int getFramesNum() const;
    virtual QString getFileName() const;

protected:
    int currFrameNum_, framesNum_;
    QString fileName_;

};

#endif // CAPTURE_H
