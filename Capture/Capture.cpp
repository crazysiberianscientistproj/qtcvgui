#include "Capture.h"

Capture::Capture(const QString &path, QObject *parent):
    QObject(parent)
  , currFrameNum_(0)
  , framesNum_(0)
{

}

Capture::~Capture()
{

}

int Capture::getCurrFrameNum() const
{
    return currFrameNum_+1;
}

void Capture::setCurrFrameNum(int currFrameNum)
{
    currFrameNum_ = currFrameNum-1;
}

int Capture::getFramesNum() const
{
    return framesNum_;
}

QString Capture::getFileName() const
{
    return fileName_;
}
