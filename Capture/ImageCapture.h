#ifndef IMAGECAPTURE_H
#define IMAGECAPTURE_H

#include "Capture.h"

class ImageCapture : public Capture
{
    Q_OBJECT

public:
    ImageCapture(const QString &path, QObject *parent=0);

    bool open(const QString &path) override;
    QImage getFrame() override;

private:
    QImage frame_;
};

#endif // IMAGECAPTURE_H
