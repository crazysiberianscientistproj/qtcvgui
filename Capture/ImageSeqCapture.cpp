#include <QDebug>
#include <QDir>

#include "ImageSeqCapture.h"
#include "FileFilters.h"

ImageSeqCapture::ImageSeqCapture(const QString &path, QObject *parent):
    Capture(path, parent)
{
    this->open(path);
}

bool ImageSeqCapture::open(const QString &path)
{
    QDir dir(path, QString(), QDir::Name | QDir::IgnoreCase, QDir::Files);
    if(!dir.exists()) return false;
    currFrameNum_=0;
    filesNames_.clear();
    dir.setNameFilters(FileFilters::IMAGESEXT);
    filesNames_=dir.entryList();
    framesNum_=filesNames_.size();
    if(framesNum_==0)return false;
    for(auto &fname:filesNames_) fname=path+"/"+fname;
    fileName_=filesNames_[0];

//    qDebug()<<filesNames;
    return true;
}

QImage ImageSeqCapture::getFrame()
{
    QImage tmpImg(fileName_);

    ++currFrameNum_;
    if(currFrameNum_==framesNum_) currFrameNum_=0;
    fileName_=filesNames_[currFrameNum_];


    return tmpImg;
}

void ImageSeqCapture::setCurrFrameNum(int currFrameNum)
{
    Capture::setCurrFrameNum(currFrameNum);
    fileName_=filesNames_[currFrameNum_];
}
