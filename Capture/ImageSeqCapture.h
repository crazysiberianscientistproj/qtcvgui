#ifndef IMAGESEQPLAYER_H
#define IMAGESEQPLAYER_H

#include <QStringList>

#include "Capture.h"

class ImageSeqCapture : public Capture
{
    Q_OBJECT

public:
    ImageSeqCapture(const QString &path, QObject *parent=0);

    bool open(const QString &path) override;
    QImage getFrame() override;
    void setCurrFrameNum(int currFrameNum) override;

private:
    QStringList filesNames_;
};

#endif // IMAGESEQPLAYER_H
